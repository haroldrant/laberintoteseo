/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Quasi
 */
public class Pair{
    private int fila; 
    private int columna;
    
    public Pair(){}
    
    public Pair(int fila, int columna){
        this.fila = fila;
        this.columna = columna;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int first) {
        this.fila = first;
    }

    public int getColumna() {
        return columna;
    }

    public void setSecond(int second) {
        this.columna = second;
    }
    
    @Override
    public String toString(){
        String msg = new String();
        msg = "("+fila + "," + columna+")";
        return msg;
    }    
}
