/**
 *
 * @author Felipe Alferez
 *         Harold Rueda
 */
package Negocio;

import Modelo.Pair;
import Util.*;

public class Laberinto {

    private short laberinto[][];
    //private ArrayList<ArrayList<Pair>> cola = new ArrayList<>();
    private ListaCD<ListaCD<Pair>> caminos = new ListaCD();
    private ListaCD<String> caminoPosible = new ListaCD();
    private Pair teseo, minotauro, salida, movimientos[];

    public Laberinto(String url) {
        this.cargarDatos(url);
    }

    private void cargarDatos(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        int f = v.length;
        int c = v[0].toString().split(";").length;
        laberinto = new short[f][c];
        for (Integer i = 0; i < f; i++) {
            String fila[] = v[i].toString().split(";");
            for (Integer j = 0; j < c; j++) {
                short data = Short.parseShort(fila[j]);
                this.laberinto[i][j] = data;
                if (data == 9) {
                    teseo = new Pair(i, j);
                } else if (data == 100) {
                    minotauro = new Pair(i, j);
                } else if (data == 1000) {
                    salida = new Pair(i, j);
                }
            }
        }
        this.cargarMovimientos();
    }

    private void cargarMovimientos() {
        this.movimientos = new Pair[4];
        this.movimientos[0] = new Pair(-1, 0); //arriba
        this.movimientos[1] = new Pair(0, -1);  //derecha
        this.movimientos[2] = new Pair(1, 0);  //abajo
        this.movimientos[3] = new Pair(0, 1); //izquierda
    }

    public String getCamino() {
        ListaCD<Pair> camino = new ListaCD();
        camino.insertarAlFinal(teseo);
        getCamino(this.teseo.getFila(), this.teseo.getColumna(), this.minotauro.getFila(), this.minotauro.getColumna(),
                this.salida.getFila(), this.salida.getColumna());
        if(!this.caminoPosible.esVacia() && this.laberinto[this.minotauro.getFila()][this.minotauro.getColumna()] == -100
                && this.laberinto[this.salida.getFila()][this.salida.getColumna()] == -1000)
            return this.caminoPosible.get(0) + this.caminoPosible.get(1);
        else
            return "No tiene solución";
    }

    private void getCamino(int filaTeseo, int colTeseo, int filaM, int colM, int filaSalida, int colSalida) {
        ListaCD<Pair> camino = new ListaCD();
        camino.insertarAlFinal(teseo);
        teseoToMinotauro(filaTeseo, colTeseo, filaM, colM, camino);
        if(this.laberinto[this.minotauro.getFila()][this.minotauro.getColumna()] == -100){
            this.mejorCamino(1);
            camino.vaciar();
            this.caminos.vaciar();
            minotauroToSalida(filaM, colM, filaSalida, colSalida, camino);
            if(this.laberinto[this.salida.getFila()][this.salida.getColumna()] == -1000)
                this.mejorCamino(0);
        }
    }
    
    private void teseoToMinotauro(int filaTeseo, int colTeseo, int filaM, int colM, ListaCD<Pair> camino){
        if(filaTeseo == filaM && colTeseo == colM){
            this.laberinto[filaM][colM] = -100;
            this.caminos.insertarAlFinal(camino.copia());
        }
        else{
            int fila,columna;
            Pair aux;
            boolean mover = false;
            for(int i = 0; i < this.movimientos.length;i++){
                fila = filaTeseo + this.movimientos[i].getFila();
                columna = colTeseo + this.movimientos[i].getColumna();
                aux = new Pair(fila,columna);
                switch(i){
                    case 0:
                        mover = this.moverArriba(filaTeseo, colTeseo, 9);
                        break;
                    case 3:
                        mover = this.moverDerecha(filaTeseo, colTeseo, 9);
                        break;
                    case 2:
                        mover = this.moverAbajo(filaTeseo, colTeseo, 9);
                        break;
                    case 1:
                        mover = this.moverIzquierda(filaTeseo, colTeseo, 9);
                        break;
                }
                if(mover){
                    camino.insertarAlFinal(aux);
                    if(this.laberinto[filaTeseo][colTeseo] != 9)
                        this.laberinto[filaTeseo][colTeseo] = 1;
                    teseoToMinotauro(aux.getFila(),aux.getColumna(),filaM, colM, camino);
                    if(this.laberinto[filaTeseo][colTeseo] != 9)
                        this.laberinto[filaTeseo][colTeseo] = 0;
                    camino.remove(aux);
                }
            }
        }
    }
    
    private void minotauroToSalida(int filaM, int colM, int filaS, int colS, ListaCD<Pair> camino){
        if(filaM == filaS && colM == colS){
            this.laberinto[filaM][colM] = -1000;
            this.caminos.insertarAlFinal(camino.copia());
        }
        else{
            int fila,columna;
            Pair aux;
            boolean mover = false;
            for(int i = 0; i < this.movimientos.length;i++){
                fila = filaM + this.movimientos[i].getFila();
                columna = colM + this.movimientos[i].getColumna();
                aux = new Pair(fila,columna);
                switch(i){
                    case 0:
                        mover = this.moverArriba(filaM, colM, -100);
                        break;
                    case 1:
                        mover = this.moverDerecha(filaM, colM, -100);
                        break;
                    case 2:
                        mover = this.moverAbajo(filaM, colM, -100);
                        break;
                    case 3:
                        mover = this.moverIzquierda(filaM, colM, -100);
                        break;
                }
                if(mover){
                    camino.insertarAlFinal(aux);
                    if(this.laberinto[filaM][colM] != -100)
                        this.laberinto[filaM][colM] = 1;
                    minotauroToSalida(aux.getFila(),aux.getColumna(),filaS, colS, camino);
                    if(this.laberinto[filaM][colM] != -100)
                        this.laberinto[filaM][colM] = 0;
                    camino.remove(aux);
                }
            }
        }
    }

    private boolean moverArriba(int filaTeseo, int colTeseo, int personaje) {
        if (posValida(filaTeseo - 1, colTeseo) && laberinto[filaTeseo - 1][colTeseo] != -1 
                && laberinto[filaTeseo - 1][colTeseo] != 1 && laberinto[filaTeseo - 1][colTeseo] != personaje) {
            return true;
        }
        return false;
    }

    private boolean moverDerecha(int filaTeseo, int colTeseo, int personaje) {
        if (posValida(filaTeseo, colTeseo + 1) && laberinto[filaTeseo][colTeseo + 1] != -1 
                && laberinto[filaTeseo][colTeseo + 1] != 1 && laberinto[filaTeseo][colTeseo + 1] != personaje) {
            return true;
        }
        return false;
    }

    private boolean moverAbajo(int filaTeseo, int colTeseo, int personaje) {
        if (posValida(filaTeseo + 1, colTeseo) && laberinto[filaTeseo + 1][colTeseo] != -1 
                && laberinto[filaTeseo + 1][colTeseo] != 1 && laberinto[filaTeseo + 1][colTeseo] != personaje) {
            return true;
        }
        return false;
    }

    private boolean moverIzquierda(int filaTeseo, int colTeseo, int personaje) {
        if (posValida(filaTeseo, colTeseo - 1) && laberinto[filaTeseo][colTeseo - 1] != -1
                && laberinto[filaTeseo][colTeseo - 1] != 1 && laberinto[filaTeseo][colTeseo - 1] != personaje) {
            return true;
        }
        return false;
    }   

    private boolean posValida(int x, int y) {
        if (x >= 0 && x < laberinto.length && y >= 0 && y < laberinto[0].length) {
            return true;
        }
        return false;
    }
    
    private void mejorCamino(int inicio){
        int menor=this.caminos.get(0).getTamanio(),actual,pos=0,i=0;
        for (ListaCD<Pair> l : caminos) {
            actual = l.getTamanio();
            if(actual < menor){
                menor = actual;
                pos = i;
            }
            i++;
        }
        rellenar(this.caminos.get(pos), inicio);
        this.caminoPosible.insertarAlFinal(this.caminos.get(pos).toString());
    }
    
    private void rellenar(ListaCD<Pair> camino, int inicio){
        for(int i=inicio;i<camino.getTamanio()-1;i++){
            this.laberinto[camino.get(i).getFila()][camino.get(i).getColumna()] = 1;
        }
    }

    @Override
    public String toString() {
        String msg = new String();
        for (short v[] : laberinto) {
            for (short data : v) {
                msg += data + "\t\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public String impCaminos() {
        String msg = new String();
        for (ListaCD<Pair> l : caminos) {
            msg += l.toString() + "\n";
        }
        return msg;
    }
}
